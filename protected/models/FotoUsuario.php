<?php

/**
 * This is the model class for table "foto_usuario".
 *
 * The followings are the available columns in table 'foto_usuario':
 * @property string $id
 * @property string $nombre_foto
 * @property string $direccion_foto
 * @property string $descripcion
 * @property string $formulario_id
 */
class FotoUsuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'foto_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre_foto, direccion_foto,  formulario_id', 'required'),
			array('nombre_foto, direccion_foto, descripcion', 'length', 'max'=>255),
			array('formulario_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre_foto, direccion_foto, descripcion, formulario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre_foto' => 'Nombre Foto',
			'direccion_foto' => 'Direccion Foto',
			'descripcion' => 'Descripcion',
			'formulario_id' => 'Formulario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nombre_foto',$this->nombre_foto,true);
		$criteria->compare('direccion_foto',$this->direccion_foto,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('formulario_id',$this->formulario_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FotoUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
