<?php 
/**
* Helper class custom
*/
class JHelper
{
	
	public static function getTipoPersona(){
		return array(1=>'Encuestador',2=>'Supervisor');
	}

	public static function getTipoPersonaText($i){
		$array=JHelper::getTipoPersona();
		return isset($array[$i])?$array[$i]:"Desconocido {$i}";
	}

	public static function getGenero()
	{
		return array(' '=>'Seleccione una opción','M'=>'Masculino','F'=>'Femenino');
	}

	public static function getGeneroText($i)
	{
		$array=JHelper::getGenero();
		return isset($array[$i]) ? $array[$i] : "Desconocido {$i}";
	}

	public static function getEstadoCivil()
	{
		return array(' '=>'Seleccione una opción','C'=>'Casado','S'=>'Soltero','U'=>'Union Libre','V'=>'Viudo','D'=>'Divorciado');
	}

	public static function getEstadoCivilText($i)
	{
		$array=JHelper::getEstadoCivil();
		return isset($array[$i]) ? $array[$i] : "Desconocido {$i}";
	}

	public static function getPreguntaSiNo()
	{
		return array(' '=>'Seleccione una opción','C'=>'Si','N'=>'No');
	}

	public static function getPreguntaSiNoText($i)
	{
		$array=JHelper::getPreguntaSiNo();
		return isset($array[$i]) ? $array[$i] : "Desconocido {$i}";
	}

	public static function getPreguntaSiNo1()
	{
		return array(' '=>'Seleccione una opción','S'=>'Si','N'=>'No');
	}

	public static function getPreguntaSiNo1Text($i)
	{
		$array=JHelper::getPreguntaSiNo();
		return isset($array[$i]) ? $array[$i] : "Desconocido {$i}";
	}
}
 ?>