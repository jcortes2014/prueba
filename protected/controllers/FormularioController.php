<?php

class FormularioController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'create', 'index', 'view', 'delete', 'update','upload'),
                'expression' => 'Yii::app()->user->checkAccess("Admin1")',
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionUpload()
	{
	 
        Yii::import("ext.EAjaxUpload.qqFileUploader");
 
        $folder=Yii::getPathOfAlias('webroot').'/fotos/';// folder for uploaded files
        $allowedExtensions = array("jpg","jpeg","gif","png");//array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 100 * 1024 * 1024;// maximum file size in bytes
        $uploader  = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result    = $uploader->handleUpload($folder);
        $return    = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
 
        $fileSize  = filesize($folder.$result['filename']);//GETTING FILE SIZE
        $fileName  = $result['filename'];//GETTING FILE NAME
        //$img = CUploadedFile::getInstance($model,'image');
 
        echo $return;// it's array
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Formulario;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Formulario']))
		{
			$model->attributes=$_POST['Formulario'];
			$login = $model->usuario;
			$clave   = md5($model->clave);
			$usu     = new User;
			$usu->nombre = $model->nombres;
			$usu->password = $clave;
			$usu->username = $login;
			$usu->save();
			
			$command = Yii::app()->db->createCommand();
			$command->insert('AuthAssignment', array(
			    'itemname'=>'usuario',
			    'userid'=>$login,
			));

			$model->user_id = $usu->id;
			if($model->save())
			{
				$fotos   = explode(',',$model->fotos);
				for($i = 0; $i<count($fotos); $i++)
				{
					$fotoUsu = new FotoUsuario;
					$fotoUsu->formulario_id = $model->id;
					$fotoUsu->nombre_foto = $fotos[$i];
					$fotoUsu->direccion_foto = Yii::getPathOfAlias('webroot').'/fotos/'.$fotos[$i];
					$fotoUsu->save();

				}
				 $this->mailsend($model->usuario,'juan.cortes.c@gmail.com','envio de correos','ha sido creado el usuario');
				$this->redirect(array('view','id'=>$model->id));
				 
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	private function mailsend($to,$from,$subject,$message)
	{
        $mail=Yii::app()->Smtpmail;
        $mail->SetFrom($from, 'From NAme');
        $mail->Subject    = $subject;
        $mail->MsgHTML($message);
        $mail->AddAddress($to, "");
        if(!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }else {
            echo "Message sent!";
        }
        $mail->ClearAddresses(); //clear addresses for next email sending
    }

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Formulario']))
		{
			$model->attributes=$_POST['Formulario'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Formulario');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Formulario('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Formulario']))
			$model->attributes=$_GET['Formulario'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Formulario the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=Formulario::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param Formulario $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='formulario-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}