<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'		=> 'Prueba',
	'language'	=> 'es',
	'theme' 	=> 'bootstrap3',
	'aliases'=>array(
		'bootstrap'=>'ext.yiibootstrap3'
		),
	// preloading 'log' component
	'preload'=>array('log','kint'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		// 'application.modules.rights.*',
		// 'application.modules.rights.components.*',
		'application.vendors.phpexcel.PHPExcel',
		'application.modules.JCGridView',
		'application.modules.JCLinkPager',
		// 'bootstrap.*',
		'bootstrap.behaviors.*',
		'bootstrap.helpers.*',
		'bootstrap.widgets.*',

		'application.extensions.EAjaxUpload.*',
		'application.extensions.coco.*', // cargar varios archivos
		),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'jauth',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths' => array('bootstrap.jgii'),
			),

		// 'rights'=>array(
		// 	'install'=>false,
		// 	),

		),

	// application components
	'components'=>array(

		'Smtpmail'=>array(
            'class'=>'application.extensions.smtpmail.PHPMailer',
            'Host'=>"smtp.gmail.com",
            'Username'=>'juan.cortes.c@gmail.com',
            'Password'=>'passwordcorreo',
            'Mailer'=>'smtp',
            'Port'=>465,
            'SMTPAuth'=>true, 
            'SMTPSecure'=>'ssl',
        ),

		 'upload'=>array(
            'class'=>'ext.upload.components.UploadComponents'
           ),
		 
		'bootstrap'=>array(
			'class'=>'bootstrap.components.BsApi',
			),
		'session' => array(
			'class'     => 'CDbHttpSession',
            'timeout'   => 60*60*24, // 24 horas de sesion activa
            ),

		'kint' => array(
			'class' => 'ext.Kint.Kint',
			),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			// 'class'=>'RWebUser',
			),
		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID' => 'db',
			// 'class'=>'RDbAuthManager',
			),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				),
			),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=prueba',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'Asdf1234',
			'charset' => 'utf8',
			),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
			),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
					),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
			),
		),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
'params'=>array(
	'subtitulo'=>'Gestion de carnets.',
	'adminEmail'	=> 'juan.cortes.c@gmail.com.com',
	'login' 		=> array(
		
		),

	),
);