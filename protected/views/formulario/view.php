<?php
/* @var $this FormularioController */
/* @var $model Formulario */
?>

<?php
$this->breadcrumbs=array(
	'Formularios'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'Listar Formulario', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Crear Formulario', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Editar Formulario', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Eliminar Formulario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'¿Está seguro de eliminar este ítem?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Administrar Formulario', 'url'=>array('admin')),
);
?>
<p>Correo enviado Correctamente</p>
<?php echo BsHtml::pageHeader('Detalles','Formulario '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombres',
		'apellidos',
		'telefono',
		'fecha_nacimiento',
	),
)); ?>