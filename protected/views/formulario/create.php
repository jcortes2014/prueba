<?php
/* @var $this FormularioController */
/* @var $model Formulario */
?>

<?php
$this->breadcrumbs=array(
	'Formularios'=>array('index'),
	'Crear',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'Listar Formulario', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Administrar Formulario', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Crear','Formulario') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>