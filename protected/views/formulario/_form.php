<?php
/* @var $this FormularioController */
/* @var $model Formulario */
/* @var $form BSActiveForm */
?>

<script type="text/javascript">
    $(function()
    {
        $(".select2").select2({
            width:'resolve'
        });

        $("#fecha_nacimiento").datepicker({
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            showOn: "both",//button
            buttonImageOnly: false,
            showAnim: "slideDown",
            dateFormat: "yy-mm-dd",
            
        });
    });

    function guardar()
    {   
        var json = eval("(" + doc + ")");  
        if($("#fotos").val() == "")
            $("#fotos").val(json.filename);
        else
        {
            val = $("#fotos").val();
            $("#fotos").val(val+","+json.filename);
        }

    }
</script>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'formulario-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Los campos con <span class="required">*</span> son obligatorios.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'nombres',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'apellidos',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'telefono',array('maxlength'=>20)); ?>
    <?php echo $form->textFieldControlGroup($model,'fecha_nacimiento',array('id'=>'fecha_nacimiento','maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'fotos',array('id'=>'fotos','maxlength'=>11)); ?>

    <?php
     $this->widget('ext.EAjaxUpload.EAjaxUpload',
    array(
            'id'=>'uploadFile',
            'config'=>array(
                   'action'=>Yii::app()->createUrl('formulario/upload'),
                   'allowedExtensions'=>array("jpg","jpeg","gif","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
                   'sizeLimit'=>1000*1024*1024,// maximum file size in bytes
                   'minSizeLimit'=>1*1024,
                   'auto'=>true,
                   'multiple' => true,
                   'onComplete'=>"js:function(id, fileName, responseJSON){
                             doc = JSON.stringify(responseJSON);
                            guardar();  }",
                   'messages'=>array(
                                     'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
                                    'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
                                    'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                                    'emptyError'=>"{file} is empty, please select files again without it.",
                                    'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                                   ),
                   'showMessage'=>"js:function(message){ alert(message); }"
                   )
     
                   ));
    ?>


    <?php echo $form->textFieldControlGroup($model,'usuario',array('maxlength'=>150)); ?>
    <?php echo $form->passwordFieldControlGroup($model,'clave',array('maxlength'=>150)); ?>
    <?php //echo $form->dropDownListControlGroup($model, 'user_id', User::model()->getUsers(), array('id' => 'user_id', 'class'=>'select select2 demo ',  'empty' => 'Seleccione su usuario',  'style' => 'width: 80%;')); ?>

    <?php echo BsHtml::submitButton('Enviar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
