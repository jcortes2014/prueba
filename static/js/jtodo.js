var app = angular.module('appcarnet', []);


app.controller('carnetsColectivo', function($scope) 
{
  $scope.ultimaFila=1;
  // $scope.totalgeneral=0;
  $scope.solicitudes = [
  {
    fila:1,
    cedula:null
  }];
  
  $scope.addSolicitud = function(){
    $scope.ultimaFila++;
    $scope.solicitudes.push({
      fila:$scope.ultimaFila,
      cedula:null
    });
  };
  
  $scope.delSolicitud = function ( fila ) {
    var solicitudes=[];
    // var $scope.solicitudes=[];
    angular.forEach($scope.solicitudes, function(solicitud){
      if(solicitud.fila!=fila){
        solicitudes.push(solicitud);
      }
    });
    $scope.solicitudes=solicitudes;
  };
  
 
});

app.controller('refLaborales', function($scope) 
{
  $scope.ultimaFila=1;
  // $scope.totalgeneral=0;
  $scope.solicitudes = [
  {
    fila:1,
    compania:null, 
    nombre:null,
    cargo:null,
    telefono:null,
    persona_contacto:null
  }];
  
  $scope.addSolicitud = function(){
    $scope.ultimaFila++;
    $scope.solicitudes.push({
      fila:$scope.ultimaFila,
      compania:null, 
      nombre:null,
      cargo:null,
      telefono:null
    });
  };
  
  $scope.delSolicitud = function ( fila ) {
    var solicitudes=[];
    // var $scope.solicitudes=[];
    angular.forEach($scope.solicitudes, function(solicitud){
      if(solicitud.fila!=fila){
        solicitudes.push(solicitud);
      }
    });
    $scope.solicitudes=solicitudes;
  };
  
 
});

app.controller('refPersonales', function($scope) 
{
  $scope.ultimaFila=1;
  // $scope.totalgeneral=0;
  $scope.solicitudes1 = [
  {
    fila:1,
    nombre:null,
    ocupacion:null,
    telefono:null
  }];
  
  $scope.addSolicitud = function(){
    $scope.ultimaFila++;
    $scope.solicitudes1.push({
      fila:$scope.ultimaFila,
      nombre:null,
      ocupacion:null,
      telefono:null
    });
  };
  
  $scope.delSolicitud = function ( fila ) {
    var solicitudes=[];
    // var $scope.solicitudes=[];
    angular.forEach($scope.solicitudes1, function(solicitud){
      if(solicitud.fila!=fila){
        solicitudes.push(solicitud);
      }
    });
    $scope.solicitudes1=solicitudes;
  };
  
 
});