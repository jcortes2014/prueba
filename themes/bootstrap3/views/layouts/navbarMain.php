<?php
$this->widget('bootstrap.widgets.BsNavbar', array(
	'collapse' => true,
	'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
	'brandUrl' => Yii::app()->homeUrl,
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.BsNav',
			'type' => 'navbar',
			'activateParents' => true,
			'items' => array(
				array(
					'label' => 'Permisos',
					'url' => array(
						'#'
						),
					'visible' => Yii::app()->user->checkAccess("Admin1"),
					'items' => array(
						BsHtml::menuHeader(BsHtml::icon(BsHtml::GLYPHICON_BOOKMARK), array(
							'class' => 'text-center',
							'style' => 'color:#99cc32;font-size:32px;'
							)),
						array(
							'label' => 'Usuarios',
							'url' => array(
								'/user/index'
								)
							),
						array(
							'label' => 'Items',
							'url' => array(
								'/jauth/authItem/index'
								)
							),
						array(
							'label' => 'Asignaciones',
							'url' => array(
								'/jauth/authAssignment/index',
								'view' => 'about'
								)
							),
						array(
							'label' => 'Jerarquias',
							'url' => array(
								'/jauth/authItemChild/index'
								)
							),
						)
					),
				array(
					'label' => 'Administrar',
					'visible' => Yii::app()->user->checkAccess("Admin1"),
					'url' => array(
						'#'
						),
					'items' => array(
						array(
							'label' => 'usuario',
							'url' => array(
								'/formulario/index'
								)
							),
						
						)
					),
				
				)
),
array(
	'class' => 'bootstrap.widgets.BsNav',
	'type' => 'navbar',
	'activateParents' => true,
	'items' => array(
		array(
			'label' => 'Login',
			'url' => array(
				'/site/login'
				),
			'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
			'visible' => Yii::app()->user->isGuest,
			'icon'=>BsHtml::GLYPHICON_USER
			),
		array(
			'label' => 'Logout (' . Yii::app()->user->name . ')',
			'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
			'url' => array(
				'/site/logout'
				),
			'visible' => !Yii::app()->user->isGuest,
			'icon'=>BsHtml::GLYPHICON_USER
			)
		),
	'htmlOptions' => array(
		'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT
		)
	)

)
));
?>