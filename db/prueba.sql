-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "AuthAssignment" ---------------------------
CREATE TABLE `AuthAssignment` ( 
	`itemname` VarChar( 64 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`userid` VarChar( 64 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`bizrule` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`data` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	 PRIMARY KEY ( `itemname`,`userid` )
 )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- CREATE TABLE "AuthItem" ---------------------------------
CREATE TABLE `AuthItem` ( 
	`name` VarChar( 64 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`type` Int( 11 ) NOT NULL, 
	`description` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`bizrule` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`data` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	 PRIMARY KEY ( `name` )
 )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- CREATE TABLE "AuthItemChild" ----------------------------
CREATE TABLE `AuthItemChild` ( 
	`parent` VarChar( 64 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, 
	`child` VarChar( 64 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	 PRIMARY KEY ( `parent`,`child` )
 )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- CREATE TABLE "User" -------------------------------------
CREATE TABLE `User` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL, 
	`username` VarChar( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`password` VarChar( 254 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
	`nombre` VarChar( 500 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	 PRIMARY KEY ( `id` )
 )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 24;
-- ---------------------------------------------------------


-- CREATE TABLE "formulario" -------------------------------
CREATE TABLE `formulario` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL, 
	`nombres` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	`apellidos` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	`telefono` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	`fecha_nacimiento` Date NOT NULL, 
	`user_id` Int( 11 ) NOT NULL,
	 PRIMARY KEY ( `id` )
, 
	CONSTRAINT `unique_user_id` UNIQUE( `user_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- ---------------------------------------------------------


-- CREATE TABLE "foto_usuario" -----------------------------
CREATE TABLE `foto_usuario` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL, 
	`nombre_foto` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	`direccion_foto` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	`descripcion` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, 
	`formulario_id` Int( 11 ) UNSIGNED NOT NULL,
	 PRIMARY KEY ( `id` )
 )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- ---------------------------------------------------------


-- Dump data of "AuthAssignment" ---------------------------
INSERT INTO `AuthAssignment`(`itemname`,`userid`,`bizrule`,`data`) VALUES ( 'Admin1', 'admin', NULL, NULL );
INSERT INTO `AuthAssignment`(`itemname`,`userid`,`bizrule`,`data`) VALUES ( 'usuario', 'juan', NULL, NULL );
-- ---------------------------------------------------------


-- Dump data of "AuthItem" ---------------------------------
INSERT INTO `AuthItem`(`name`,`type`,`description`,`bizrule`,`data`) VALUES ( 'Admin1', '1', NULL, NULL, NULL );
INSERT INTO `AuthItem`(`name`,`type`,`description`,`bizrule`,`data`) VALUES ( 'usuario', '1', NULL, NULL, NULL );
-- ---------------------------------------------------------


-- Dump data of "AuthItemChild" ----------------------------
-- ---------------------------------------------------------


-- Dump data of "User" -------------------------------------
INSERT INTO `User`(`id`,`username`,`password`,`nombre`) VALUES ( '22', 'admin', 'fa5a02c9cc183b3ff1bfcd4c2243f85c', 'juan carlos' );
INSERT INTO `User`(`id`,`username`,`password`,`nombre`) VALUES ( '26', 'juan', '202cb962ac59075b964b07152d234b70', 'juan' );
-- ---------------------------------------------------------


-- Dump data of "formulario" -------------------------------
INSERT INTO `formulario`(`id`,`nombres`,`apellidos`,`telefono`,`fecha_nacimiento`,`user_id`) VALUES ( '1', 'juan', 'jj', 'jkas', '0000-00-00', '26' );
-- ---------------------------------------------------------


-- Dump data of "foto_usuario" -----------------------------
INSERT INTO `foto_usuario`(`id`,`nombre_foto`,`direccion_foto`,`descripcion`,`formulario_id`) VALUES ( '1', 'xxx', 'xxx', 'xxx', '0' );
INSERT INTO `foto_usuario`(`id`,`nombre_foto`,`direccion_foto`,`descripcion`,`formulario_id`) VALUES ( '2', 'AuditoriaEscalonada90.jpg', '/usr/local/apache2/htdocs/prueba/fotos/AuditoriaEscalonada90.jpg', NULL, '1' );
INSERT INTO `foto_usuario`(`id`,`nombre_foto`,`direccion_foto`,`descripcion`,`formulario_id`) VALUES ( '3', 'Afiche_sabe63.jpg', '/usr/local/apache2/htdocs/prueba/fotos/Afiche_sabe63.jpg', NULL, '1' );
-- ---------------------------------------------------------


-- CREATE INDEX "child" ------------------------------------
CREATE INDEX `child` USING BTREE ON `AuthItemChild`( `child` );
-- ---------------------------------------------------------


-- CREATE LINK "AuthAssignment_ibfk_1" ---------------------
ALTER TABLE `AuthAssignment` ADD CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY ( `itemname` ) REFERENCES `AuthItem`( `name` ) ON DELETE Cascade ON UPDATE Cascade;
-- ---------------------------------------------------------


-- CREATE LINK "AuthItemChild_ibfk_1" ----------------------
ALTER TABLE `AuthItemChild` ADD CONSTRAINT `AuthItemChild_ibfk_1` FOREIGN KEY ( `parent` ) REFERENCES `AuthItem`( `name` ) ON DELETE Cascade ON UPDATE Cascade;
-- ---------------------------------------------------------


-- CREATE LINK "AuthItemChild_ibfk_2" ----------------------
ALTER TABLE `AuthItemChild` ADD CONSTRAINT `AuthItemChild_ibfk_2` FOREIGN KEY ( `child` ) REFERENCES `AuthItem`( `name` ) ON DELETE Cascade ON UPDATE Cascade;
-- ---------------------------------------------------------


-- CREATE LINK "lnk_formulario_User" -----------------------
ALTER TABLE `formulario` ADD CONSTRAINT `lnk_formulario_User` FOREIGN KEY ( `user_id` ) REFERENCES `User`( `id` ) ON DELETE No Action ON UPDATE Cascade;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


